import Vue from 'vue'
import App from './App'
import helper from "./common/helper.js";
Vue.prototype.webtitle = "易风课堂"
Vue.prototype.weburl ="http://www.yifengkt.cn"
Vue.prototype.helper = helper
Vue.config.productionTip = false

App.mpType = 'app'

const app = new Vue({
    ...App
})
app.$mount()
